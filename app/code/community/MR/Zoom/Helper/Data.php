<?php
class MR_Zoom_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getModuleConfig($key)
    {
        return Mage::getStoreConfig('mr_zoom/' . $key);
    }
}