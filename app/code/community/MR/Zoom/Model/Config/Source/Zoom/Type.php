<?php
class MR_Zoom_Model_Config_Source_Zoom_Type{
    public function toOptionArray(){
        return array(
            array(
                'value' => 'window',
                'label' => 'Window'
            ),
            array(
                'value' => 'inner',
                'label' => 'Inner'
            )
        );
    }
}